#Pwn My App

##Setup

assuming you have docker installed <br>

`git clone` <br>
`cd pwn_my_app` (where docker-compose.yml is located) <br>

`docker-compose build` (build the app) <br> 
`docker-compose up -d` (run the containers)<br>

`docker exec -it www_docker_symfony bash` <br>
(get inside /www shell to run symfony commands)<br>

`cd project` (you should be here now : '/var/www/project#' )<br>
`php bin/console doctrine:database:create` (create db)<br>

`php bin/console make:migration` (run migrations to update db)<br>
`php bin/console doctrine:migrations:migrate` <br>

##In your browser
pma can be accessed here : http://127.0.0.1:8080/

project homepage here : http://127.0.0.1:8741/

##XSS
Only xss is exploitable atm

##SQLi
wip

##CSRF
wip

